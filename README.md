**Products API**
----
**Run tests:** mvn test

**Run application:** mvn spring-boot:run

<_The application will run on http://localhost:8080/products_>

<_On-line https://codeavenue.herokuapp.com/products_>


Endpoint|Methods|Optional|
--- | --- | --- |
|/products | `GET` , `POST` , `DELETE` , `PUT`  | withImage=boolean&WithProducts=boolean|  
|/products/{productId}|`GET` ,  `DELETE` |
|/products/{productId}/images|`GET` , `POST` , `DELETE` , `PUT`|
|/products/{productId}/images/{imageId}|`GET` ,  `DELETE` |
|/products/{productId}/products|`GET` , `POST` , `DELETE` , `PUT`|
|/products/{productId}/products|`GET` ,  `DELETE` |
  
  
*  **Example: POST /products - Request body**

    ```json
    {
    	"name":"Glass",
    	"description":"Wine Glass"
    }
    ```
    
*  **Example: POST /products/{productId} - Request body**

    ```json
    {
    	"name":"Glass",
    	"description":"Wine Glass"
    }
    ```


*  **Example: POST /products/{productId}/images - Request body**

    ```json
   {
	"type":"jpg",
	"url":"http://www.wineglass.com.br/sample.png"
    }
    ```
 
* **Error Response:**

     ```json
    {
    "status": "NOT_FOUND",
    "developerMessage": "[stacktrace]",
    "message": "HTTP 404 Not Found"
    }
    ```

