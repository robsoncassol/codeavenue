package com.codeavenue.cassol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CassolApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(CassolApplication.class, args);
	}
}
