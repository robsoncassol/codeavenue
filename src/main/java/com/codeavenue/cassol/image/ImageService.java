package com.codeavenue.cassol.image;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageService {

	@Autowired
	private ImageRepository imageRepository;

	@Transactional
	public Image findOne(Long id) {
		return imageRepository.findOne(id);
	}

	@Transactional
	public Image save(Image image) {
		return imageRepository.save(image);
	}

	@Transactional
	public Image update(Image image) {
		if(image.getId()==null) {
    		throw new NotFoundException();
    	}
		return imageRepository.save(image);
	}

}
