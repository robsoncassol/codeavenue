package com.codeavenue.cassol.image;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.codeavenue.cassol.product.repository.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Image {

	@Id
	@GeneratedValue
	@Column(unique=true)
	private Long id;

	private String url;
	
	private String type;

	@JsonIgnore
	@ManyToOne
	private Product product;
	
	public Image() {
	}

	public Image(String url, String type) {
		
	}
	public Image(String url, Long productId) {
		super();
		this.product = new Product(productId);
		this.url = url;
	}

	public Image(String url, Product product) {
		super();
		this.product = product;
		this.url = url;
	}

	public Image(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Long getId() {
		return id;
	}

	
	public String getType() {
		return type;
	}

	
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Image [id=" + id + ", url=" + url + ", type=" + type + ", product=" + product + "]";
	}
	
	
	

}
