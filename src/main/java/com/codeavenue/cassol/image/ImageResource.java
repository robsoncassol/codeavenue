package com.codeavenue.cassol.image;

import java.util.Collection;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.codeavenue.cassol.product.ProductDTO;
import com.codeavenue.cassol.product.ProductService;
import com.codeavenue.cassol.product.repository.Product;

@Path("/products/{productId}/images")
@Produces("application/json")
public class ImageResource {

	private final ImageService imageService;
	private final ProductService productService;

	@Inject
	public ImageResource(ImageService imageService, ProductService productService) {
		this.imageService = imageService;
		this.productService = productService;
	}
	

    @GET
    public Collection<Image> listAllImages(@PathParam("productId") Long productId) {
    	ProductDTO product = productService.findOne(productId);
        return product.getImages();
    }

    @Path("/{id}")
    @GET
    public Image getImage(@PathParam("productId") Long productId,@PathParam("id") Long id) throws NotFoundException {
    	productService.findOneAndThrowExceptionIfNotExist(productId);
        return imageService.findOne(id);
    }

    @POST
    public Image addImage(@PathParam("productId") Long productId, Image image) {
    	Product product = productService.findOneAndThrowExceptionIfNotExist(productId);
    	image.setProduct(product);
        return imageService.save(image);
    }
    
    
    @PUT
    public Image updateImage(Image image) throws NotFoundException {
        return imageService.update(image);
    }


    @Path("/{id}")
    @DELETE
    public void delete(@PathParam("productId") Long productId, @PathParam("id") Long id) {
        productService.removeImage(productId,id);
    }

    @DELETE
    @Transactional
    public void deleteAll(@PathParam("productId") Long productId) {
    	productService.deleteAllImages(productId);
    }


}
