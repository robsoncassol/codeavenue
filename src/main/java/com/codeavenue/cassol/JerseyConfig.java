package com.codeavenue.cassol;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.codeavenue.cassol.config.GenericExceptionMapper;
import com.codeavenue.cassol.image.ImageResource;
import com.codeavenue.cassol.product.ChildProductResource;
import com.codeavenue.cassol.product.ProductResource;

@Configuration
public class JerseyConfig extends ResourceConfig {
	
    public JerseyConfig() {
//    	packages(getClass().getPackage().getName());
        register(ProductResource.class);
        register(ChildProductResource.class);
        register(ImageResource.class);
        register(GenericExceptionMapper.class);
    }
}
