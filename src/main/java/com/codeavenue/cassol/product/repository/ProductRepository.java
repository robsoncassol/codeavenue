package com.codeavenue.cassol.product.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProductRepository extends JpaRepository<Product, Long> ,JpaSpecificationExecutor<Product> {

	Product findByName(String name);
	
	List<Product> findAllByParent(Product product);
	
	Product findByParentAndId(Product product, Long id);

	List<Product> findAllByParentIsNull();

}
