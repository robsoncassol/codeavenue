package com.codeavenue.cassol.product.repository;

import static javax.persistence.criteria.JoinType.LEFT;
import static org.springframework.util.StringUtils.isEmpty;

import org.springframework.data.jpa.domain.Specification;

public class QProduct {
	
	
	public static Specification<Product> fetchImages(Boolean b) {
		return (root, query, cb) -> {
			if(b!=null && b)
				root.fetch("images",LEFT);
			return null;
		};
	}
	
	public static Specification<Product> fetchProducts(Boolean b) {
		return (root, query, cb) -> {
			if(b!=null && b)
				root.fetch("products",LEFT);
			return null;
		};
	}
	
	public static Specification<Product> parentEqual(Product value) {
		return (root, query, cb) -> {
			if(value==null)
				return null;

			return cb.equal(root.get("parent"), value);
		};
	}
	
	
	public static Specification<Product> likeDescription(String value) {
		return (root, query, cb) -> {
			if(isEmpty(value))
				return null;

			return cb.like(cb.lower(root.get("description")), "%" + value.toLowerCase()+"%");
		};
	}

	public static Specification<Product> parentIsNull() {
		return (root, query, cb) -> {
			return cb.isNull(root.get("parent"));
		};
	}

	public static Specification<Product> idEqual(Long id) {
		return (root, query, cb) -> {
			if(isEmpty(id))
				return null;

			return cb.equal(root.get("id"), id);
		};
	}

	public static Specification<Product> distinct() {
		return (root, query, cb) -> {
			query.distinct(true);
			return null;
		};
	}


}
