package com.codeavenue.cassol.product.repository;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.codeavenue.cassol.image.Image;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

	@Id
	@GeneratedValue
	@Column(unique=true)
	private Long id;

	private String name;

	private String description;

	@OneToMany(cascade = CascadeType.ALL, mappedBy="product", orphanRemoval=true)
	private Set<Image> images = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy="parent", orphanRemoval=true)
	private Set<Product> products = new HashSet<>();

	@JsonIgnore
	@ManyToOne
	private Product parent;

	public Product() {
	}

	public Product(Long id) {
		this.id = id;
	}

	public Product(String name) {
		super();
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "[" + id + "," + name + "]";
	}

	public void addImage(Image image) {
		if(image==null)
			return;
		image.setProduct(this);
		this.images.add(image);
	}

	public void addProduct(Product product) {
		if (product != null && this.equals(product))
 			throw new RuntimeException("[Product] Circular reference");
		product.setParent(this);
		this.products.add(product);
	}

	public Set<Product> getProducts() {
		return products;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setParent(Product parent) {
		if (parent.equals(this))
			throw new RuntimeException("[Product] Circular reference");
		this.parent = parent;
	}

	public void deleteAllProducts() {
		this.products.clear();
	}

	public Product getParent() {
		return parent;
	}

	public void deleteAllImages() {
		this.images.clear();
	}

	public Set<Image> getImages() {
		return images;
	}

	public void removeImage(Long imageId) {
		images.removeIf(i -> i.getId().equals(imageId));
	}

	public void removeProduct(Long productId) {
		products.removeIf(p -> p.getId().equals(productId));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		return true;
	}
	
	

}
