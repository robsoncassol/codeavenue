package com.codeavenue.cassol.product;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.codeavenue.cassol.product.repository.Product;

@Path("/products/{parentId}/products")
@Produces("application/json")
public class ChildProductResource {

	private final ProductService productService;

	@Inject
	public ChildProductResource(ProductService productService) {
		this.productService = productService;
	}
	

    @GET
    public List<ProductDTO> listProducts(@PathParam("parentId") Long parentId) {
        ProductDTO parent = productService.findOne(parentId);
		return parent.getProducts();
    }

    @Path("/{id}")
    @GET
    public ProductDTO getProduct(@PathParam("parentId") Long parentId,@PathParam("id") Long id) throws NotFoundException {
        return productService.findOne(id);
    }

    @POST
    public ProductDTO createProduct(@PathParam("parentId") Long parentId, Product product) {
        return productService.addProduct(parentId, product);
    }
    
    
    @PUT
    public ProductDTO updateProduct(Product product) throws NotFoundException {
        return productService.update(product);
    }


    @Path("/{id}")
    @DELETE
    public void delete(@PathParam("parentId") Long parentId, @PathParam("id") Long productId) {
        productService.removeProduct(parentId,productId);
    }

    @DELETE
    public void deleteAll(@PathParam("parentId") Long parentId) {
    	productService.deleteAllProducts(parentId);
    }


}
