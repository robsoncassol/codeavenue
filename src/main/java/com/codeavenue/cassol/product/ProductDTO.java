package com.codeavenue.cassol.product;

import static java.util.stream.Collectors.toList;
import static jersey.repackaged.com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.List;

import com.codeavenue.cassol.image.Image;
import com.codeavenue.cassol.product.repository.Product;

public class ProductDTO {
	
	private Long id;

	private String name;

	private String description;

	private List<Image> images = new ArrayList<>();

	private List<ProductDTO> products = new ArrayList<>();

	public ProductDTO(Product product, Boolean withImages, Boolean withProducts) {
		this.id = product.getId();
		this.name = product.getName();
		this.description = product.getDescription();
		if(withImages)
			this.images = newArrayList(product.getImages());
		if(withProducts)
			this.products = product.getProducts().stream().map(p -> new ProductDTO(p,withImages,withProducts)).collect(toList());
	}

	
	public ProductDTO(Product product) {
		this(product ,true, true);
	}


	public Long getId() {
		return id;
	}

	
	public String getName() {
		return name;
	}

	
	public String getDescription() {
		return description;
	}

	
	public List<Image> getImages() {
		return images;
	}

	
	public List<ProductDTO> getProducts() {
		return products;
	}
	
	

}
