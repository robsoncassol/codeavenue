package com.codeavenue.cassol.product;

import static com.codeavenue.cassol.product.repository.QProduct.fetchImages;
import static com.codeavenue.cassol.product.repository.QProduct.fetchProducts;
import static com.codeavenue.cassol.product.repository.QProduct.parentIsNull;
import static java.util.stream.Collectors.toList;
import static org.springframework.data.jpa.domain.Specifications.where;

import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codeavenue.cassol.product.repository.Product;
import com.codeavenue.cassol.product.repository.ProductRepository;
import com.codeavenue.cassol.product.repository.QProduct;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;

	@Transactional
	public ProductDTO findOne(Long id) {
		Product product = findOneAndThrowExceptionIfNotExist(id);
		return new ProductDTO(product);
	}

	public void deleteAll() {
		productRepository.deleteAll();
	}

	public Product findByParentAndId(Product product, Long id) {
		return productRepository.findOne(where(QProduct.parentEqual(product))
											.and(QProduct.idEqual(id)));
	}

	@Transactional
	public void deleteAllImages(Long id) {
		Product product = findOneAndThrowExceptionIfNotExist(id);
        product.deleteAllImages();
	}

	@Transactional
	public List<ProductDTO> findAllFirstLevelProducts(Boolean withImages, Boolean withProducts) {
		List<Product> products = productRepository.findAll(where(parentIsNull())
				.and(fetchImages(withProducts))
				.and(fetchProducts(withImages))
				.and(QProduct.distinct()));
		return products.stream().map(p -> new ProductDTO(p,withImages,withProducts)).collect(toList());
	}

	@Transactional
	public void delete(Long id) {
		Product product = findOneAndThrowExceptionIfNotExist(id);
		productRepository.delete(product);
	}

	@Transactional
	public ProductDTO addProduct(Long parentId, Product product) {
		Product parent = findOneAndThrowExceptionIfNotExist(parentId);
        parent.addProduct(product);
        return new ProductDTO(parent);
	}

	public Product findOneAndThrowExceptionIfNotExist(Long parentId) {
		Product parent = productRepository.findOne(parentId);
        if (parent == null) {
            throw new NotFoundException();
        }
		return parent;
	}

	@Transactional
	public void removeProduct(Long parentId, Long productId) {
		Product parent = findOneAndThrowExceptionIfNotExist(parentId);
		parent.removeProduct(productId);
	}

	@Transactional
	public void deleteAllProducts(Long parentId) {
		Product parent = findOneAndThrowExceptionIfNotExist(parentId);
		parent.deleteAllProducts();
	}

	@Transactional
	public void removeImage(Long productId, Long imageId) {
		Product parent = findOneAndThrowExceptionIfNotExist(productId);
		parent.removeImage(imageId);
	}

	public ProductDTO save(Product product) {
		return new ProductDTO(productRepository.save(product));
	}

	public ProductDTO update(Product product) {
		if (product.getId() == null) {
			throw new NotFoundException();
		}
		return save(product);
	}

}
