package com.codeavenue.cassol.product;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.codeavenue.cassol.product.repository.Product;

@Path("/products")
@Produces("application/json")
public class ProductResource {

	private final ProductService productService;

	@Inject
	public ProductResource(ProductService productRepository) {
		this.productService = productRepository;
	}

	@GET
	@Transactional
	public List<ProductDTO> listProducts(@DefaultValue("false") @QueryParam("withImages") Boolean withImages,
										 @DefaultValue("false") @QueryParam("withProducts") Boolean withProducts) {

		return productService.findAllFirstLevelProducts(withImages, withProducts);
	}

	@GET
	@Path("/{id}")
	public ProductDTO getProduct(@PathParam("id") Long id) throws NotFoundException {
		return productService.findOne(id);
	}

	@POST
	@Consumes("application/json")
	public ProductDTO createProduct(Product product) {
		return productService.save(product);
	}

	@PUT
	@Consumes("application/json")
	public ProductDTO updateProduct(Product product) throws NotFoundException {
		return productService.update(product);
	}

	@DELETE
	@Path("/{id}")
	public void delete(@PathParam("id") Long id) {
		productService.delete(id);
	}

	@DELETE
	public void deleteAll() {
		productService.deleteAll();
		;
	}
	
	

}
