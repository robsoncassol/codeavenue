package com.codeavenue.cassol.config;

import javax.ws.rs.core.Response.Status;

public class ErrorMessage {

	private int status;
	private String developerMessage;
	private String message;

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}
	
	public String getDeveloperMessage() {
		return developerMessage;
	}

	public Status getStatus() {
		return Status.fromStatusCode(status);
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	

}
