package com.codeavenue.cassol.product;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.codeavenue.cassol.product.repository.Product;
import com.codeavenue.cassol.product.repository.ProductRepository;

import jersey.repackaged.com.google.common.collect.Lists;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ChildProductResourceTest {
	
	private static final String PATH = "/products/%d/products";
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private ProductRepository productRepository;
	
	
	@Test
	public void shouldListAll() {
		Product product = productRepository.save(new Product());
		String path = String.format(PATH, product.getId());
		restTemplate.delete(path);
		String body = this.restTemplate.getForObject(path, String.class);
		assertThat(body).isEqualTo("[]");
	}
	
	@Test
	public void shouldReturnListWithOneProductAndItsSubProducs() {
		productRepository.deleteAll();
		Product parent = new Product("parent-1");
		parent.addProduct(new Product("child"));
		productRepository.save(parent);

		ParameterizedTypeReference<List<Product>> responseType = new ParameterizedTypeReference<List<Product>>() {};
		ResponseEntity<List<Product>> response = restTemplate.exchange("/products?withImages=true&withProducts=true",HttpMethod.GET, null, responseType);
		assertThat(response.getBody().isEmpty()).isFalse();
		assertThat(response.getBody().get(0).getProducts().isEmpty()).isFalse();
	}
	
	@Test
	public void shouldReturnListWithOneProductAndNoSubProducs() {
		Product parent = new Product("parent-2");
		parent.addProduct(new Product("child"));
		productRepository.save(parent);

		ParameterizedTypeReference<List<Product>> responseType = new ParameterizedTypeReference<List<Product>>() {};
		ResponseEntity<List<Product>> response = restTemplate.exchange("/products",HttpMethod.GET, null, responseType);
		assertThat(response.getBody().isEmpty()).isFalse();
		assertThat(response.getBody().get(0).getProducts().isEmpty()).isTrue();
	}
	

	@Test
	public void shouldGetProduct() {
		Product product = new Product();
		product.setParent(productRepository.save(new Product("parent")));
		Product child = productRepository.save(product);
		String path = String.format(PATH, child.getParent().getId());
		
		Product savedProduct = this.restTemplate.getForObject(path + "/"+child.getId(),Product.class);
		assertThat(savedProduct.getId()).isEqualTo(child.getId());
	}

	@Test
	public void testCreateProduct() {
		Product parent = productRepository.save(new Product("parent"));
		String path = String.format(PATH, parent.getId());

		HttpEntity<Product> request = new HttpEntity<>(new Product("bar"));
		Product response = restTemplate.postForObject(path, request, Product.class);
		
		assertThat(response.getId()).isNotNull();
	}

	@Test
	public void shouldUpdateSubProduct() {
		Product product = new Product();
		product.setParent(productRepository.save(new Product("parent")));
		Product child = productRepository.save(product);
		String path = String.format(PATH, child.getParent().getId());
		child.setDescription("EDITED");
		
		HttpEntity<Product> request = new HttpEntity<>(child);
		restTemplate.put(path, request);
		Product response = restTemplate.getForObject(path+"/"+child.getId(), Product.class);
		assertThat(response.getDescription()).isEqualTo("EDITED");
	}

	@Test
	public void shouldDeleteOneSubProduct() {
		Product parent = createProductWithTwoSubProducts();
		String path = String.format(PATH, parent.getId());
		
		restTemplate.delete(path+"/"+getFirstSubProduct(parent).getId());
		
		Product savedProduct = this.restTemplate.getForObject("/products/"+parent.getId(),Product.class);
		assertThat(savedProduct.getProducts().size()).isEqualTo(1);
	}

	private Product getFirstSubProduct(Product parent) {
		return Lists.newArrayList(parent.getProducts()).get(0);
	}

	@Test
	public void shouldDeleteAllSubProducts() {
		Product parent = createProductWithTwoSubProducts();
		String path = String.format(PATH, parent.getId());
		restTemplate.delete(path);
		Product savedProduct = this.restTemplate.getForObject("/products/"+parent.getId(),Product.class);
		assertThat(savedProduct.getProducts().isEmpty()).isTrue();
	}

	private Product createProductWithTwoSubProducts() {
		Product parent = productRepository.save(new Product("parent"));
		parent.addProduct(new Product("child1"));
		parent.addProduct(new Product("child2"));
		parent = productRepository.save(parent);
		return parent;
	}

}
