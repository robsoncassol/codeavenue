package com.codeavenue.cassol.product;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.codeavenue.cassol.image.Image;
import com.codeavenue.cassol.image.ImageRepository;
import com.codeavenue.cassol.product.repository.Product;
import com.codeavenue.cassol.product.repository.ProductRepository;


@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductRepositoryTest {
	
	
	private static final String PRODUCT_NAME_1 = "Smartphone";
	private static final String PRODUCT_NAME_2 = "Smartwatch";
	
	@Autowired
	private ProductRepository productRepository;
	
	
	@Autowired
	private ImageRepository imageRepository;
	
	
	@Test
	public void shouldSaveProduct() {
		productRepository.save(new Product(PRODUCT_NAME_1));
		List<Product> products = productRepository.findAll();
		assertFalse(products.isEmpty());
	}
	
	@Test
	public void shouldUpdateProduct() {
		productRepository.save(new Product(PRODUCT_NAME_1));
		Product product = productRepository.findByName(PRODUCT_NAME_1);
		product.setName(PRODUCT_NAME_2);
		Product updatedProduct = productRepository.save(product);
		assertEquals(PRODUCT_NAME_2, updatedProduct.getName());
	}

	@Test
	public void shouldDeleteProduct() {
		productRepository.save(new Product(PRODUCT_NAME_1));
		Product product = productRepository.findByName(PRODUCT_NAME_1);
		productRepository.delete(product);
		List<Product> products = productRepository.findAll();
		assertTrue(products.isEmpty());
	}
	
	@Test
	public void shouldSaveProductAndImages() {
		imageRepository.deleteAll();
		Product product = new Product(PRODUCT_NAME_1);
		product.addImage(new Image());
		product.addImage(new Image());
		productRepository.save(product);
		Assert.assertEquals(2,imageRepository.findAll().size());
	}
	
	@Test
	public void shouldDeleteProductAndAllImages() {
		Product product = new Product(PRODUCT_NAME_1);
		product.addImage(new Image());
		product.addImage(new Image());
		productRepository.save(product);
		productRepository.delete(product);
		assertTrue(imageRepository.findAll().isEmpty());
	}
	
	@Test
	public void shouldSaveProductAndChildProducts() {
		Product product = new Product(PRODUCT_NAME_1);
		product.addProduct(new Product());
		Product savedProduct = productRepository.save(product);
		assertFalse(savedProduct.getProducts().isEmpty());
	}
	
	@Test
	public void shouldDeleteProductAndChildProducts() {
		Product product = new Product(PRODUCT_NAME_1);
		product.addProduct(new Product());
		Product savedProduct = productRepository.save(product);
		productRepository.delete(savedProduct);
		assertTrue(productRepository.findAll().isEmpty());
	}
	
	@Test(expected=RuntimeException.class)
	public void shouldThrowExceptionWhenAddTheSameProductOnChildProduct() {
		Product product = new Product(PRODUCT_NAME_1);
		product.addProduct(product);
	}

	@Test(expected=RuntimeException.class)
	public void shouldThrowExceptionWhenAddTheSameProductToParent() {
		Product product = new Product(PRODUCT_NAME_1);
		product.setParent(product);
	}
	
	

}
