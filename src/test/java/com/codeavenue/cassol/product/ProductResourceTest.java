package com.codeavenue.cassol.product;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.codeavenue.cassol.product.repository.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@Transactional
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductResourceTest {

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void shouldReturnEmptyList() {
		restTemplate.delete("/products/");
		String body = this.restTemplate.getForObject("/products", String.class);
		assertThat(body).isEqualTo("[]");
	}
	
	@Test
	public void shouldReturnListWithOneProduct() {
		restTemplate.delete("/products/");
		HttpEntity<Product> request = new HttpEntity<>(new Product("bar"));
		restTemplate.postForObject("/products", request, Product.class);
		
		ParameterizedTypeReference<List<Product>> responseType = new ParameterizedTypeReference<List<Product>>() {};
		ResponseEntity<List<Product>> response = restTemplate.exchange("/products",HttpMethod.GET, null, responseType);
		assertThat(response.getBody().isEmpty()).isFalse();
	}
	
	@Test
	public void shouldSaveProduct() {
		HttpEntity<Product> request = new HttpEntity<>(new Product("bar"));
		Product product = restTemplate.postForObject("/products", request, Product.class);
		assertThat(product.getId()).isNotNull();
	}
	
	@Test
	public void shouldDeleteSpecificProduct() {
		HttpEntity<Product> request = new HttpEntity<>(new Product("bar"));
		Product foo = restTemplate.postForObject("/products", request, Product.class);
		restTemplate.delete("/products/"+foo.getId());
		ResponseEntity<Product> entity = this.restTemplate.getForEntity("/products/"+foo.getId(), Product.class);
		assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}
	
	
	@Test
	public void shouldDeleteAllProducts() {
		HttpEntity<Product> request = new HttpEntity<>(new Product("bar"));
		restTemplate.postForObject("/products", request, Product.class);
		restTemplate.postForObject("/products", request, Product.class);
		restTemplate.delete("/products/");
		String body = this.restTemplate.getForObject("/products", String.class);
		assertThat(body).isEqualTo("[]");
	}
	
	
	@Test
	public void shouldEditProduct() {
		HttpEntity<Product> request = new HttpEntity<>(new Product("TV"));
		Product tv = restTemplate.postForObject("/products", request, Product.class);
		assertThat(tv.getDescription()).isNull();
		tv.setDescription("TV LED");
		restTemplate.put("/products", new HttpEntity<>(tv), Product.class);

		Product product = restTemplate.getForObject("/products/"+tv.getId(), Product.class);
		assertThat(product.getDescription()).isEqualTo("TV LED");
	}
	
	@Test
	public void shouldThrowExceptionWhenTryEditProductWithoutId() {
		HttpEntity<Product> request = new HttpEntity<>(new Product("TV"));
		Product tv = restTemplate.postForObject("/products", request, Product.class);
		assertThat(tv.getDescription()).isNull();
		tv.setDescription("TV LED");
		Product product = restTemplate.postForObject("/products", new HttpEntity<>(tv), Product.class);
		assertThat(product.getDescription()).isEqualTo("TV LED");
	}
	

}
