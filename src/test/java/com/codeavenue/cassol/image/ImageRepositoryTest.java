package com.codeavenue.cassol.image;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.codeavenue.cassol.product.repository.Product;
import com.codeavenue.cassol.product.repository.ProductRepository;


@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ImageRepositoryTest {
	
	
	private static final String URL_1 = "http://s3.codeavenu.com/robsoncasso/1";
	private static final String URL_2 = "http://s3.codeavenu.com/robsoncasso/2";

	private static final String PRODUCT_NAME_1 = "Smartphone";
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ImageRepository imageRepository;
	
	
	@Test
	public void shouldSaveImage() {
		Product product = productRepository.save(new Product(PRODUCT_NAME_1));
		imageRepository.save(new Image(URL_1,product));
		List<Image> images = imageRepository.findAll();
		assertFalse(images.isEmpty());
		assertNotNull(images.get(0).getProduct());
	}
	
	@Test
	public void shouldDeleteImage() {
		Product product = productRepository.save(new Product(PRODUCT_NAME_1));
		imageRepository.save(new Image(URL_1,product));
		List<Image> images = imageRepository.findAll();
		imageRepository.delete(images);
		assertTrue(imageRepository.findAll().isEmpty());
	}
	
	@Test
	public void shouldUpdateImage() {
		Product product = productRepository.save(new Product(PRODUCT_NAME_1));
		imageRepository.save(new Image(URL_1,product));
		
		Image image = imageRepository.findByUrl(URL_1);
		image.setUrl(URL_2);
		Image updatedImage = imageRepository.save(image);
		
		assertEquals(URL_2,updatedImage.getUrl());
	}
	
	

}
