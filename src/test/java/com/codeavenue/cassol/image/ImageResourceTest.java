package com.codeavenue.cassol.image;

import static jersey.repackaged.com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.codeavenue.cassol.product.repository.Product;
import com.codeavenue.cassol.product.repository.ProductRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ImageResourceTest {
	
	private static final String PATH = "/products/%d/images";
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ImageRepository imageRepository;
	
	@Test
	public void shouldListAll() {
		Product product = productRepository.save(new Product());
		String path = String.format(PATH, product.getId());
		restTemplate.delete(path);
		String body = this.restTemplate.getForObject(path, String.class);
		assertThat(body).isEqualTo("[]");
	}
	
	@Test
	public void shouldReturnListWithOneProductAndItsSubProducs() {
		productRepository.deleteAll();
		Product product = new Product("parent-1");
		product.addImage(new Image("jpg"));
		productRepository.save(product);

		ParameterizedTypeReference<List<Product>> responseType = new ParameterizedTypeReference<List<Product>>() {};
		ResponseEntity<List<Product>> response = restTemplate.exchange("/products?withImages=true&withProducts=true",HttpMethod.GET, null, responseType);
		assertThat(response.getBody().isEmpty()).isFalse();
		assertThat(response.getBody().get(0).getImages().isEmpty()).isFalse();
	}
	
	@Test
	public void shouldReturnListWithOneProductAndNoImages() {
		Product product = new Product("product-2");
		product.addImage(new Image("png"));
		productRepository.save(product);

		ParameterizedTypeReference<List<Product>> responseType = new ParameterizedTypeReference<List<Product>>() {};
		ResponseEntity<List<Product>> response = restTemplate.exchange("/products",HttpMethod.GET, null, responseType);
		assertThat(response.getBody().isEmpty()).isFalse();
		assertThat(response.getBody().get(0).getImages().isEmpty()).isTrue();
	}
	

	@Test
	public void shouldGetImage() {
		Image image = new Image();
		image.setProduct(productRepository.save(new Product("parent")));
		image = imageRepository.save(image);
		String path = String.format(PATH, image.getProduct().getId());
		
		Image savedImage = this.restTemplate.getForObject(path + "/"+image.getId(),Image.class);
		assertThat(savedImage.getId()).isEqualTo(image.getId());
	}

	@Test
	public void testAddNewImage() {
		Product product = productRepository.save(new Product("parent"));
		String path = String.format(PATH, product.getId());

		HttpEntity<Image> request = new HttpEntity<>(new Image("jpg"));
		Image response = restTemplate.postForObject(path, request, Image.class);
		
		assertThat(response.getId()).isNotNull();
	}

	@Test
	public void testUpdateImage() {
		Image image = new Image();
		image.setProduct(productRepository.save(new Product("parent")));
		image = imageRepository.save(image);
		String path = String.format(PATH, image.getProduct().getId());
		image.setUrl("EDITED");
		HttpEntity<Image> request = new HttpEntity<>(image);
		restTemplate.put(path, request);
		Image response = restTemplate.getForObject(path+"/"+image.getId(), Image.class);
		assertThat(response.getUrl()).isEqualTo("EDITED");
	}

	@Test
	public void shouldDeleteOneImage() {
		Product parent = createProductWithTwoImages();
		String path = String.format(PATH, parent.getId());
		
		restTemplate.delete(path+"/"+getFirstImage(parent).getId());
		
		Product savedProduct = this.restTemplate.getForObject("/products/"+parent.getId(),Product.class);
		assertThat(savedProduct.getImages().size()).isEqualTo(1);
	}

	private Image getFirstImage(Product parent) {
		return newArrayList(parent.getImages()).get(0);
	}

	@Test
	public void shouldDeleteAllImages() {
		Product product = createProductWithTwoImages();
		String path = String.format(PATH, product.getId());
		restTemplate.delete(path);
		Product savedProduct = this.restTemplate.getForObject("/products/"+product.getId(),Product.class);
		assertThat(savedProduct.getImages().isEmpty()).isTrue();
	}

	private Product createProductWithTwoImages() {
		Product product = productRepository.save(new Product("parent"));
		product.addImage(new Image("gif"));
		product.addImage(new Image("tif"));
		return productRepository.save(product);
	}

}
